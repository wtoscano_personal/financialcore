INSTRUCCIONES DE CÓMO EJECUTAR LA APLICACIÓN EN UN AMBIENTE LOCAL
	1. Descargar la solucion y ubicar el archivo \\BlueBank_FinancialCore\BlueBank_FinancialCore\Web.config
		Ubicar las llaves listadas a continuacion y colocar los datos de una base SQL Server a la que tengan acceso en su ambiente local
		<add key="DataServer" value="BD_BLUEBANK" />
		<add key="DataUser" value="dbuser" />
		<add key="DataPassword" value="dbpss" />
		<add key="AAADataContext_Catalogue" value="BLUEBANK" />

	2. Dentro de esta base de datos (SQL Server) ejecutar el script ubicado en la dirección 
		 \\BD OBJECTS\CreateTable_Accounts.sql modificando el Catálogo (USE <nombre BD>)
	
	3. Una vez el proyecto haya compilado correctamente --> F5 --> Copiar la URL que se carga en el navegador (se recomienda CHROME)

	4. Abrir POSTMAN o INSOMNIA
		Crear un nuevo Request
		Colocar la URL copiada en el paso anterior y guiarse por el documento de pruebas de acuerdo a cada metodo
	
	5. Validar los cambios en la nueva tabla creada en la base de datos SQL Server


SUPUESTOS DE NEGOCIO Y TECNOLOGÍA QUE REALIZÓ PARA SOLUCIONAR EL PROBLEMA

	1. API REST .NET ENTITY FRAMEWORK
	2. Persistencia de la Información
	3. Tiempos de respuesta inmediatos
	4. En caso de que la información ingresada no sea correcta se maneja adecuadamente la respuesta y excepciones dentro del servicio


EXPLICACIÓN DE LA ARQUITECTURA QUE PLANTEO PARA SOLUCIONAR EL PROBLEMA
	Se diseña una solución API REST para mitigar altos tiempos de resupesta en un mercado competitivo, aplicando un mecanismo de persistencia 
	de información relacional pasando por métodos en capas que permiten mantener la confidencialidad de los datos.
	
	Se aplica este tipo de arquitectura de desarrollo web ya que se apoya totalmente en el estándar HTTP y el aislamiento que proporciona 
	entre la lógica del back-end y cualquier cliente consumidor del servicio.
	
	Se aplican los métodos tipo POST y GET que nos proporciona una interfaz uniforme para la transferencia de datos en el sistema REST 
	aplicando operaciones concretas sobre un recurso determinado en este caso sobre la tabla Accounts.
	

EXPLICACIÓN DE LAS TECNOLOGÍAS SELECCIONADAS PARA RESOLVER EL PROBLEMA
	Las aplicaciones .NET pueden ser multiplataforma
	Está orientado a obtener el mejor rendimiento y tiempo de respuesta


QUÉ HARÍA MEJOR O COMO PODRÍA ATACAR MEJOR EL PROBLEMA SI TUVIERA MÁS TIEMPO
	Mejor manejo de excepciones y errores en la respuesta del API 
