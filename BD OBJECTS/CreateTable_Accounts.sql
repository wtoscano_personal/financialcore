USE [BLUEBANK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]'))
	DROP TABLE [dbo].[Accounts]
GO
CREATE TABLE [dbo].[Accounts](
	[id_account] [int] IDENTITY(1,1) NOT NULL,
	[first_last_name] [varchar](250) NOT NULL,
	[account_number] [varchar](5) NOT NULL,
	[total_amount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[id_account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
