﻿using BlueBank_Data;
using BlueBank_Data.Models;
using BlueBank_Models;
using BlueBank_Models.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BlueBank_Managers.Managers
{
    public partial class AccountManager
    {
		public static async Task<ManagerResponse<System.String>> CreateAccount(AccountsModel model)
		{
			var ret = new ManagerResponse<System.String>();
			var resp = model.Validate();
			Random rd = new Random();
			string accountNumber = "";

			if (!resp.Success)
			{
				return ret.Add(resp);
			}
			using (var db = new AAADataContext())
			{
				var newEntity = new Accounts();
				//entity.id_account = int.Parse(model.id_account.ToString());
				newEntity.first_last_name = model.first_last_name;
				newEntity.account_number = rd.Next(100,1000).ToString();
				newEntity.total_amount = model.total_amount;
				db.Accounts.Add(newEntity);
				await Utils.SaveChangesAsync(db);
				accountNumber = newEntity.account_number;
				ret.SetData(newEntity.account_number);
			}
			if (ret.Success) { ret.SuccessMessages.Add("Your account were successfully created. Your account number is "+ accountNumber); }
			return ret;
		}

		public static async Task<ManagerResponse<System.String>> Consign(AccountsModel model)
		{
			var ret = new ManagerResponse<System.String>();
			var retObj = new AccountsModel();

			using (var db = new AAADataContext())
			{
				var entity = await db.Accounts.FirstOrDefaultAsync(x => x.account_number == model.account_number);
				if (entity == null)
				{
					return ret.Add("No account was found with that number", ResponseErrorCodes.ResourceNotFound);
				}
				entity.total_amount = entity.total_amount + model.total_amount;
				await Utils.SaveChangesAsync(db);
			}
			
			if (ret.Success) { ret.SuccessMessages.Add("Your account were successfully consigned by " + model.total_amount); }
			return ret;
		}

		public static async Task<ManagerResponse<System.String>> Withdrawals(AccountsModel model)
		{
			var ret = new ManagerResponse<System.String>();
			var retObj = new AccountsModel();

			using (var db = new AAADataContext())
			{
				var entity = await db.Accounts.FirstOrDefaultAsync(x => x.account_number == model.account_number);
				if (entity == null)
				{
					return ret.Add("No account was found with that number", ResponseErrorCodes.ResourceNotFound);
				}
				entity.total_amount = entity.total_amount - model.total_amount;
				await Utils.SaveChangesAsync(db);
			}

			if (ret.Success) { ret.SuccessMessages.Add("It has made a withdrawal from your checking account by " + model.total_amount); }
			return ret;

		}

		public static async Task<ManagerResponse<System.Decimal>> AccountBalance(AccountsModel model)
		{
			var ret = new ManagerResponse<System.Decimal>();
			var retObj = new AccountsModel();
			decimal total_amount = 0;

			using (var db = new AAADataContext())
			{
				var entity = await db.Accounts.FirstOrDefaultAsync(x => x.account_number == model.account_number);
				if (entity == null)
				{
					return ret.Add("No account was found with that number", ResponseErrorCodes.ResourceNotFound);
				}
				entity.account_number = model.account_number;
				total_amount = entity.total_amount;
				ret.SetData(entity.total_amount);
			}
			if (ret.Success) { ret.SuccessMessages.Add("Your account balance was sucefully got "+ total_amount.ToString()); }
			return ret;
		}

	}
}
