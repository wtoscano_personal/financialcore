﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueBank_Models
{
    public class ResourceAccessResponse
    {
        public int? PK_Id_Int32 { get; set; }
        public Guid? PK_Id_Guid { get; set; }
        public bool Valid { get; set; }
    }
}
