﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueBank_Models.Models
{
    public partial class AccountsModel
    {
		public string ListDisplay { get; set; }
		[Key]
		public System.Int32? id_account { get; set; }

		[Required]
		public System.String first_last_name { get; set; }

		[Required]
		public System.String account_number { get; set; }

		[Required]
		public System.Decimal total_amount { get; set; }

		public ManagerResponse<object> Validate()
		{
			var ret = new ManagerResponse<object>();
			if (string.IsNullOrEmpty(first_last_name))
			{
				ret.Add("Name is required.");
			}
			if ((total_amount <= 0))
			{
				ret.Add("An amount is required.");
			}
			return ret;
		}


	}
}
