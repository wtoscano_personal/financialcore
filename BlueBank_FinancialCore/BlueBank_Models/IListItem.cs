﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueBank_Models
{
    public interface IListItem
    {

        string ListDisplay { get; set; }

        bool Selected { get; set; }
    }

    public class ListItem
    {
        public object Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }

    public class ListItemByte : IListItem
    {
        public Byte PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }

    public class ListItemInt32 : IListItem
    {
        public Int32 PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }
    public class ListItemInt16 : IListItem
    {
        public Int16 PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }
    public class ListItemInt64 : IListItem
    {
        public Int64 PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }

    public class ListItemGuid : IListItem
    {
        public Guid PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }

    public class ListItemString : IListItem
    {
        public string PK_Id { get; set; }

        public string ListDisplay { get; set; }

        public bool Selected { get; set; }

    }

    public class CheckboxListSave
    {
        public object Id { get; set; }
        public List<ListItem> Items { get; set; }
    }
}
