﻿using BlueBank_Managers.Managers;
using BlueBank_Models;
using BlueBank_Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BlueBank_FinancialCore.Controllers
{
    public class AccountController : BaseApiController
	{
		[HttpPost]
		[Route("api/Account/CreateAccount")]
		public async Task<IHttpActionResult> CreateAccount(AccountsModel model)
		{
			var resp = await AccountManager.CreateAccount(model);
			return Ok<ManagerResponse<System.String>>(resp);
		}

		[HttpPost]
		[Route("api/Account/Consign")]
		public async Task<IHttpActionResult> Consign(AccountsModel model)
		{
			var resp = await AccountManager.Consign(model);
			return Ok<ManagerResponse<System.String>>(resp);
		}

		[HttpPost]
		[Route("api/Account/Withdrawals")]
		public async Task<IHttpActionResult> Withdrawals(AccountsModel model)
		{
			var resp = await AccountManager.Withdrawals(model);
			return Ok<ManagerResponse<System.String>>(resp);
		}

		[HttpGet]
		[Route("api/Account/AccountBalance")]
		public async Task<IHttpActionResult> AccountBalance(AccountsModel model)
		{
			var resp = await AccountManager.AccountBalance(model);
			return Ok<ManagerResponse<System.Decimal>>(resp);
		}
	}
}