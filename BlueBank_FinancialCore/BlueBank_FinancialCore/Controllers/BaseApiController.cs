﻿using BlueBank_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BlueBank_FinancialCore.Controllers
{
    public class BaseApiController: ApiController
    {
        public async Task<ResourceAccessResponse> ValidateAccess()
        {
            HttpRequestMessage re = Request;
            var headers = re.Headers;
            var ret = new ResourceAccessResponse();

            if (headers.Contains("x-token"))
            {
                string token = headers.GetValues("x-token").First();
                //var json = Crypto.DecryptStringAES(token);
                //var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginToken>(json);
                //ret = await ResourceAccess.ValidateClientResource(obj);
                if (!ret.Valid)
                {
                    throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            return ret;

        }

        protected bool ValidateCaptcha()
        {
            return true;   
        }

    }
}