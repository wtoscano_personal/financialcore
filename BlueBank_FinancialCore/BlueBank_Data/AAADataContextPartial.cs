﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BlueBank_Data.Models
{
    public partial class AAADataContext : DbContext
    {
        public AAADataContext()
           : base(ConfigSettings.AAADataContextConnectionString)
        {
        }
    }
}
