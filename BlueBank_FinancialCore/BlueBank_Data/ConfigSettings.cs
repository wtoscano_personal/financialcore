﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueBank_Data
{
    public class ConfigSettings
    {

        private static string aAADataContextConnectionString;
        public static string AAADataContextConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(aAADataContextConnectionString))
                {
                    var s = "Data Source={0};Initial Catalog={1};Persist Security Info=false;User ID={2};Password={3}";
                    var dataSource = ConfigurationManager.AppSettings.Get("DataServer");
                    var catalogue = ConfigurationManager.AppSettings.Get("AAADataContext_Catalogue");
                    var user = ConfigurationManager.AppSettings.Get("DataUser");
                    var password = ConfigurationManager.AppSettings.Get("DataPassword");
                    aAADataContextConnectionString = string.Format(s, dataSource, catalogue, user, password);
                }
                return aAADataContextConnectionString;
            }
        }
    }
}
