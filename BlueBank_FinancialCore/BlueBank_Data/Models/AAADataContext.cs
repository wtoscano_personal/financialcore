﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BlueBank_Data.Models
{
    public partial class AAADataContext : DbContext
    {
        public virtual DbSet<Accounts> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>();
//                .WithRequired(e => e.Accounts);
        }

    }

}
