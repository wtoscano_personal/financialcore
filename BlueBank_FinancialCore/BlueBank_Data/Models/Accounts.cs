﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueBank_Data.Models
{
    public partial class Accounts
    {
        [Key]
        public int id_account { get; set; }
        public string first_last_name { get; set; }
        public string account_number { get; set; }
        public decimal total_amount { get; set; }

        public Accounts() { }
    }
}
